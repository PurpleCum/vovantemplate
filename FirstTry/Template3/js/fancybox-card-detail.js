'use strict';
(function () {

	var catalog = document.querySelector('.swiper-wrapper');

	var modelName = document.querySelector('.catalog-card-detail__title');
	var modelPrice = document.querySelector('.catalog-card-detail__price span');
	var modelDesc = document.querySelector('.catalog-card-detail__info-text');

	var modelPrice_Form = document.querySelector('#catalog-card-detail-form-price');
	var modelName_Form = document.querySelector('#catalog-card-detail-form-model');
	var modelArt_Form = document.querySelector('#catalog-card-detail-form-art'); // еще нет артикулёв

	var slide_1 = document.querySelector('.__1 img');
	var slide_2 = document.querySelector('.__2 img');
	var slide_3 = document.querySelector('.__3 img');
	var slide_4 = document.querySelector('.__4 img');

	$('.catalog-item__btn-details, .catalog-item-img').on('click', function(e) {

	
		if(e.target.classList.contains('catalog-item-img') || e.target.classList.contains('catalog-item__btn-details')) {
			console.log('жмяк');

			modelName.innerHTML = e.target.closest('.catalog-item').querySelector('.catalog-item__title').innerHTML;
			modelPrice.innerHTML = e.target.closest('.catalog-item').querySelector('.catalog-item__price--price').innerHTML;
			modelDesc.innerHTML = e.target.closest('.catalog-item').querySelector('.catalog-item__img-galery--desc').innerHTML;
		
			modelPrice_Form.value = e.target.closest('.catalog-item').dataset.price;
			// modelArt_Form.value = e.target.closest('.catalog-item').dataset.art;
			modelName_Form.value = e.target.closest('.catalog-item').querySelector('.catalog-item__title').innerHTML;


			slide_1.setAttribute('src', e.target.closest('.catalog-item').querySelector('.catalog-item__img-galery--1').src);
			slide_2.setAttribute('src', e.target.closest('.catalog-item').querySelector('.catalog-item__img-galery--2').src);
			slide_3.setAttribute('src', e.target.closest('.catalog-item').querySelector('.catalog-item__img-galery--3').src);
			slide_4.setAttribute('src', e.target.closest('.catalog-item').querySelector('.catalog-item__img-galery--4').src);

			if(e.target.closest('.catalog-item').getAttribute('data-test') == 'A') {
				console.log('orange main popup');

				document.querySelector('.catalog-card-detail').setAttribute('data-test', 'A');
			}

			else if(e.target.closest('.catalog-item').getAttribute('data-test') == 'B') {
				console.log('green main popup');

				document.querySelector('.catalog-card-detail').setAttribute('data-test', 'B');
			}

		}

		$.fancybox.open({
			src  : '#catalog-card-detail',
			type : 'inline',
			touch : {
				vertical : false,  // Allow to drag content vertically
				// momentum : false   // Continuous movement when panning
			},
		
		});


		var swiperCatalogCardDetail = new Swiper('.catalog-card-detail-swiper', {
		
			slidesPerView : 1,
			spaceBetween : 100,
			speed: 400,
			// effect : 'slide',
			effect : 'flip',

			flipEffect : {
				slideShadows: false,
				rotate: 180,
			},
		
			loop: true,
			// бесконечная прокрутка
		
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
	
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
	
		});
	
	}); // click





})();
// end