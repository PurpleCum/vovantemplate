'use strict';
(function () {

	var videoBG = 'url(' + document.querySelector('.video .hidden--elements .hereImg img').getAttribute('src') + ')';
	var videoUrl = document.querySelector('.video .hidden--elements .hereSrcYouTube').innerHTML;
	
	var reasonBG = 'url(' + document.querySelector('.video .hidden--elements .hereImgReason img').getAttribute('src') + ')';

	$('.video').css({'background-image' : videoBG});
	$('.reason-header').css({'background-image' : reasonBG});
	$('.video__play-btn').attr('href', videoUrl);

})();
// end