
$(document).ready(function () {

	var catalogNi = new Swiper('.catalog-ni-item__ava--swiper-container', {
	
		slidesPerView : 1,
		spaceBetween : 40,
		speed: 400,
	
		// zoom: true,
		loop: true,
		// бесконечная прокрутка
	
		pagination: {
			el: '.swiper-pagination',
		},
	
	});

	var catalog3i = new Swiper('.catalog-3i-item__galery--container', {
	
		slidesPerView : 1,
		spaceBetween : 60,
		speed: 400,
	
		loop: true,
		// бесконечная прокрутка
	
		pagination: {
			el: '.swiper-pagination',
			type: 'fraction',
		},
	
	});

}); // документ он лоад



'use strict';
(function () {

var toggler = document.querySelector('.nav-menu__toggler');
var navMenu = document.querySelector('.nav-menu');

toggler.addEventListener('click', function () {
  navMenu.classList.toggle('nav-menu--open');
});


var suka = document.querySelector('.header-top__item');
var tel = document.querySelector('.header-top__item--main-phone');

var telShort = '<span>8 (800) 500-62-57</span>';
var telNormal = 'Бесплатно по РФ: <span>8 (800) 500-62-57</span>';

if( suka.offsetWidth < 500 ){
	tel.innerHTML = telShort;
}

window.addEventListener('resize', function(){

	console.log(suka.offsetWidth);

	if( suka.offsetWidth < 500 ){
		tel.innerHTML = telShort;
	} else {
		tel.innerHTML = telNormal;
	}

});

})();
// end