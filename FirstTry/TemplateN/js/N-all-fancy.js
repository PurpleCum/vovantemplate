





/* DOMContentLoaded - аналог "$(document).ready" */
// 'use strict';
(function () {

	$('.get-steps__item--btn').on('click', function() {
	
	  $.fancybox.open({
	    src  : '#callback-popup',
	    type : 'inline',
	
		touch : {
			vertical : false,  // Allow to drag content vertically
			// momentum : false   // Continuous movement when panning
		},
	
	  });
	
	});

	if(document.querySelector('.about-1i')){

		$('.about-1i-item__btn').on('click', function() {
		
			$.fancybox.open({
				src  : '#callback-popup',
				type : 'inline',
			
				touch : {
					vertical : false,  // Allow to drag content vertically
					// momentum : false   // Continuous movement when panning
				},
		  	});
		});
	}

  
  $('.menu__callback-btn').on('click', function() {
  
    $.fancybox.open({
      src  : '#callback-popup',
      type : 'inline',
  
    touch : {
      vertical : false,  // Allow to drag content vertically
      // momentum : false   // Continuous movement when panning
    },
  
    });
  
  });


})();
// end



'use strict';
(function () {

/*

    ПЕРЕМЕННЫЕ

*/

var catalog = document.querySelector('.catalog-ni');

var modelChar_Time = document.querySelector('.popup-catalog-item-chars__text--time');
var modelChar_Radius = document.querySelector('.popup-catalog-item-chars__text--radius');
var modelChar_Weight = document.querySelector('.popup-catalog-item-chars__text--weight');
var modelChar_Speed = document.querySelector('.popup-catalog-item-chars__text--speed');


var modelChar_Name = Array.from(document.querySelectorAll('.popup-catalog-item__title'));

var modelName = document.querySelector('.popup-catalog-item__title');
var modelName_Form = document.querySelector('#model-name');

var modelPrice = document.querySelector('.popup-catalog-item-price-list__price');
var modelPrice_Form = document.querySelector('#model-price');
var modelPriceDel = document.querySelector('.popup-catalog-item-price-list__price-del');
var modelArt_Form = document.querySelector('#model-art');

var modelPrice_form_ease_buy = document.querySelector('#popup-ease-buy-price');
var modelName_form_ease_buy = document.querySelector('#popup-ease-buy-name');


/* Задаем артикулы и цены, таща с массива значения */
var catalogItems = Array.from(document.querySelectorAll('.catalog-item'));


// el.querySelector('.catalog-item-price-list__price').innerHTML = catalogInfo[el.id].price.replace(/(\d{1,3})(?=((\d{3})*)$)/g, " $1") + ' Руб.';

$('.catalog-ni').on('click', function(e) {

  /*

    КУПИТЬ В 1 КЛИК

  */

  if (e.target.closest('.catalog-ni-item__btn')) {
    document.querySelector('#popup-buy-one-click-price').value = e.target.closest('.catalog-ni-item').querySelector('.catalog-ni-item__prices--new span').innerHTML;
    document.querySelector('#popup-buy-one-click-name').value = e.target.closest('.catalog-ni-item').querySelector('.catalog-ni-item__name').innerHTML;
    // document.querySelector('#articul').value = e.target.closest('.catalog-item').dataset.art;

    document.querySelector('.popup-buy-one-click__img').setAttribute('src', e.target.closest('.catalog-ni-item').querySelector('.swiper-slide-active').src);
    document.querySelector('.popup-buy-one-click__info--price span').innerHTML = e.target.closest('.catalog-ni-item').querySelector('.catalog-ni-item__prices--new span').innerHTML.replace(/(\d{1,3})(?=((\d{3})*)$)/g, " $1") + ' Руб.';
    document.querySelector('.popup-buy-one-click__info--model').innerHTML = e.target.closest('.catalog-ni-item').querySelector('.catalog-ni-item__name').innerHTML;

    $.fancybox.open({
      src  : '#popup-buy-one-click',
      type : 'inline',
    });

  } 

}); // делегирование на каталог


$('.catalog-3i').on('click', function(e) {

  /*

    КУПИТЬ В 1 КЛИК

  */

  if (e.target.closest('.catalog-3i-item__btn')) {
    document.querySelector('#popup-buy-one-click-price').value = e.target.closest('.catalog-3i-item').querySelector('.catalog-3i-item__prices--new span').innerHTML;
    document.querySelector('#popup-buy-one-click-name').value = e.target.closest('.catalog-3i-item').querySelector('.catalog-3i-item__name').innerHTML;
    // document.querySelector('#articul').value = e.target.closest('.catalog-item').dataset.art;

    document.querySelector('.popup-buy-one-click__img').setAttribute('src', e.target.closest('.catalog-3i-item').querySelector('.swiper-slide-active').src);
    document.querySelector('.popup-buy-one-click__info--price span').innerHTML = e.target.closest('.catalog-3i-item').querySelector('.catalog-3i-item__prices--new span').innerHTML.replace(/(\d{1,3})(?=((\d{3})*)$)/g, " $1") + ' Руб.';
    document.querySelector('.popup-buy-one-click__info--model').innerHTML = e.target.closest('.catalog-3i-item').querySelector('.catalog-3i-item__name').innerHTML;

    $.fancybox.open({
      src  : '#popup-buy-one-click',
      type : 'inline',
    });

  } 

}); // делегирование на каталог




})(); // the end
