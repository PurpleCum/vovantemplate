'use strict';
(function () {

var container = document.querySelector('.container');
var descWidth = 500.01;
var img = document.querySelector('.promo__dudes-img.__hidden');

if(+container.offsetWidth < descWidth){
	img.src = './img/promo-rockspace-eb50.png';
}

window.addEventListener('resize', function(){

	if(+container.offsetWidth < descWidth){
		img.src = './img/promo-rockspace-eb50.png';
	} else {
		img.src = './img/promo-dudes.png';
	}

});




})(); /*  ---END---  */