'use strict';
(function () {

	var container = document.querySelector('.container');
	var descWidth = 767;

	var needHover = [
		'.btn',
		'.catalog-item__btn-details',
		'.catalog-item__btn',
		'.catalog-item',
		'.popup-catalog-item__btn',
		'.nav-menu__item',
		'.nav-menu__toggler',
		'.filter-item',
		'.bullets-item__wrapper',
	];

	var string = needHover.join();
	document.querySelectorAll(string).forEach(function(el){

		if(+container.offsetWidth > descWidth){
			el.classList.add('__hover');
		}

	});

	window.addEventListener('resize', function(){

		document.querySelectorAll(string).forEach(function(el){

			if(+container.offsetWidth < descWidth){
				el.classList.remove('__hover');
			} else {
				el.classList.add('__hover');
			}
			
		});

	});

})(); // the end