'use strict';
(function () {

	var videoBG = 'url(' + document.querySelector('.video .hidden--elements .hereImg img').getAttribute('src') + ')';
	var videoUrl = document.querySelector('.video .hidden--elements .hereSrcYouTube').innerHTML;
	
	var reasonBG = 'url(' + document.querySelector('.video .hidden--elements .hereImgReason img').getAttribute('src') + ')';

	$('.video').css({'background-image' : videoBG});
	$('.reason-header').css({'background-image' : reasonBG});
	$('.video__play-btn').attr('href', videoUrl);


	if(document.querySelector('.about-1i')){

		var aboutItemBG1 = 'url(' + document.querySelector('.about-1i .hidden--elements .bg1_img img').getAttribute('src') + ')';
		var aboutItemBG2 = 'url(' + document.querySelector('.about-1i .hidden--elements .bg2_img img').getAttribute('src') + ')';
		var aboutItemBG3 = 'url(' + document.querySelector('.about-1i .hidden--elements .bg3_img img').getAttribute('src') + ')';

		$('#about-1i-item-1 .about-1i-item__img').css({'background-image' : aboutItemBG1});
		$('#about-1i-item-2 .about-1i-item__img').css({'background-image' : aboutItemBG2});
		$('#about-1i-item-3 .about-1i-item__img').css({'background-image' : aboutItemBG3});
	}

})();
// end