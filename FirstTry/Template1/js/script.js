(function () {

'use strict';

var phone = $('form input[name="phone"]').val();
var name = $('form input[name="name"]').val();

// callback from menu

$('#menu-callback-form').on('submit', function(e){
  e.preventDefault();

  $.ajax({
    type: 'POST',
    url: './main-form.php',
    data: $(this).serialize(),
    success: function(data) {

        document.getElementById('menu-callback-form').reset();

        $('.menu-callback-form__btn--text').html('УСПЕШНО!');
        $('.menu-callback-form__btn').addClass('__success');
        $('.menu-callback-form__btn').attr('type', 'reset');

        $('.menu-callback-form__input-field').addClass('__success-animation');

        $('#menu-callback-form .popup__text').html('Ваша заявка успешно отправлена! Пожалуйста, дождитесь звонка нашего специалиста');

        setTimeout( function(){

           $('.menu-callback-form__input-field').addClass('__none');

        }, 1750);

      setTimeout( function(){
        $('#menu-callback-form .popup__text').html('Наш специалист свяжется с Вами в ближайшее время и проконсультирует по всем возникшим вопросам');

        $('.menu-callback-form__input-field').removeClass('__success-animation');
        $('.menu-callback-form__input-field').removeClass('__none');


        $('.menu-callback-form__btn--text').html('Отправить');
        $('.menu-callback-form__btn').attr('type', 'submit');
        $('.menu-callback-form__btn').removeClass('__success');
      }, 10000);
    }
  })
});





// promo страничка

$('.promo-form').on('submit', function(e){
  e.preventDefault();

  $.ajax({
    type: 'POST',
    url: './main-form.php',
    data: $(this).serialize(),
    success: function(data) {

        document.querySelector('.promo-form').reset();

        $('.promo__btn .promo__btn--text.__primary').html('УСПЕШНО!');
        $('.promo__btn').addClass('__success');
        $('.promo__btn').attr('type', 'reset');

        $.fancybox.open({
          src  : '#promo-popup-thx',
          type : 'inline',
  
        }); 
        
        setTimeout( function(){
          $('.promo__btn .promo__btn--text.__primary').html('Забронировать');
          $('.promo__btn').attr('type', 'submit');
          $('.promo__btn').removeClass('__success');
        }, 10000);
    }
  })
});



// Купить в 1 клик

$('#catalog-card-buy-form').on('submit', function(e){
  e.preventDefault();

  $.ajax({
    type: 'POST',
    url: './main-form.php',
    data: $(this).serialize(),
    success: function(data) {

      document.getElementById('catalog-card-buy-form').reset();

      $('#catalog-card-buy-form').css('pointer-events','none');

      $('.catalog-card-buy__text').html('Отличный выбор! Ваша заявка успешно отправлена <br> Пожалуйста, дождитесь звонка нашего специалиста');
      $('.catalog-card-buy__text').addClass('__success');
      $('.catalog-card-buy-form').attr('type', 'reset');

      $('.catalog-card-buy-form__btn').html('Готово!');

      setTimeout( function(){
        $('#catalog-card-buy-form').css('pointer-events','auto');
        $('.catalog-card-buy-form').attr('type', 'submit');

        $('.catalog-card-buy-form__btn').html('Оформить заказ');
        
        $('.catalog-card-buy__text').removeClass('__success'); 
        $('.catalog-card-buy__text').html('Заполните форму ниже, и мы перезвоним Вам для уточнения деталей заказа');

      }, 10000);

    }
  })
});



/* callback botton + footer */

$('.buy-now-form').on('submit', function(e){
  e.preventDefault();

  $.ajax({
    type: 'POST',
    url: './main-form.php',
    data: $(this).serialize(),
    success: function(data) {

        document.querySelector('.buy-now-form').reset();

        $('.buy-now__btn--text').html('Отправлено!');
        $('.buy-now-form').css('pointer-events','none');

        $('.buy-now__btn').addClass('__success');

        $.fancybox.open({
          src  : '#promo-popup-thx',
          type : 'inline',
          opts : {
            onComplete : function() {
            }
          }
        });

        setTimeout( function(){

          $('.buy-now__btn').removeClass('__success');

          $('.buy-now__btn--text').html('Заказать');
          $('.buy-now-form').css('pointer-events','auto');

        }, 10000);
    }
  })
});



$('.callback-last-form').on('submit', function(e){
  e.preventDefault();

  $.ajax({
    type: 'POST',
    url: './main-form.php',
    data: $(this).serialize(),
    success: function(data) {

        document.querySelector('.callback-last-form').reset();

        $('.callback-last-form').css('pointer-events','none');

        $('.callback-last__btn').html('Отправлено!');
        $('.callback-last__btn').addClass('__success');

        $.fancybox.open({
          src  : '#promo-popup-thx',
          type : 'inline',
          opts : {
            onComplete : function() {
            }
          }
        });

        setTimeout( function(){

          $('.callback-last__btn').removeClass('__success');
          $('.callback-last__btn').html('Отправить');

          $('.callback-last-form').css('pointer-events','auto');

        }, 10000);
    }
  })
});









//// catalog-popup-main

$('#catalog-card-detail-form').on('submit', function(e){
  e.preventDefault();

  $.ajax({
    type: 'POST',
    url: './main-form.php',
    data: $(this).serialize(),
    success: function(data) {

        document.getElementById('catalog-card-detail-form').reset();

        $('#catalog-card-detail-form').css('pointer-events','none');
        $('.catalog-card-detail-form__btn--text').html('Готово!');

        $.fancybox.open({
          src  : '#promo-popup-thx',
          type : 'inline',
          opts : {
            onComplete : function() {
            }
          }
        });

        setTimeout( function(){
          $('#catalog-card-detail-form').css('pointer-events','auto');
          $('.catalog-card-detail-form__btn--text').html('Оформить заказ');

        }, 12000);
    }
  })
});


})();
 