'use strict';
(function () {


	var catalog = document.querySelector('.swiper-wrapper');
	var modelName = document.querySelector('.catalog-card-buy__model-name');
	var modelPrice = document.querySelector('.catalog-card-buy__model-price');
	var modelAva = document.querySelector('.catalog-card-buy__img img');

	var modelPrice_Form = document.querySelector('#catalog-card-buy-form-price');
	var modelName_Form = document.querySelector('#catalog-card-buy-form-model');
	var modelArt_Form = document.querySelector('#catalog-card-buy-form-art'); // еще нет артикулёв



	$('.catalog-item__btn').on('click', function(e) {

		modelName.innerHTML = e.target.closest('.catalog-item').querySelector('.catalog-item__title').innerHTML;
		modelPrice.innerHTML = e.target.closest('.catalog-item').querySelector('.catalog-item__price--price').innerHTML;
		modelAva.setAttribute('src', e.target.closest('.catalog-item').querySelector('.catalog-item-img').src);

		modelPrice_Form.value = e.target.closest('.catalog-item').dataset.price;
		// modelArt_Form.value = e.target.closest('.catalog-item').dataset.art;
		modelName_Form.value = e.target.closest('.catalog-item').querySelector('.catalog-item__title').innerHTML;

		if(e.target.closest('.catalog-item').getAttribute('data-test') == 'A') {
			console.log('orange popup');

			document.querySelector('.catalog-card-buy').setAttribute('data-test', 'A');
		}

		else if(e.target.closest('.catalog-item').getAttribute('data-test') == 'B') {
			console.log('green popup');

			document.querySelector('.catalog-card-buy').setAttribute('data-test', 'B');
		}

		$.fancybox.open({
			src  : '#catalog-card-buy',
			type : 'inline',
			touch : {
				vertical : false,  // Allow to drag content vertically
				// momentum : false   // Continuous movement when panning
			},
		
		});
	
	});












})();
// end