
$(document).ready(function () {

	var catalogNi = new Swiper('.catalog-ni-item__ava--swiper-container', {
	
		slidesPerView : 1,
		spaceBetween : 40,
		speed: 400,
	
		// zoom: true,
		loop: true,
		// бесконечная прокрутка
	
		pagination: {
			el: '.swiper-pagination',
		},
	
	});

	var catalog3i = new Swiper('.catalog-3i-item__galery--container', {
	
		slidesPerView : 1,
		spaceBetween : 60,
		speed: 400,
	
		loop: true,
		// бесконечная прокрутка
	
		pagination: {
			el: '.swiper-pagination',
			type: 'fraction',
		},
	
	});

}); // документ он лоад



'use strict';
(function () {

var toggler = document.querySelector('.nav-menu__toggler');
var navMenu = document.querySelector('.nav-menu');

toggler.addEventListener('click', function () {
  navMenu.classList.toggle('nav-menu--open');
});


var suka = document.querySelector('.header-top__item');
var tel = document.querySelector('.header-top__item--main-phone');

var telShort = '<span>8 (800) 500-62-57</span>';
var telNormal = 'Бесплатно по РФ: <span>8 (800) 500-62-57</span>';

if( suka.offsetWidth < 500 ){
	tel.innerHTML = telShort;
}

window.addEventListener('resize', function(){

	if( suka.offsetWidth < 500 ){
		tel.innerHTML = telShort;
	} else {
		tel.innerHTML = telNormal;
	}

});






/*

	Rewies magic swiper

*/

var container = document.querySelector('.rewies__container');
var descWidth = 1024.1;

var swiperWrapper = document.querySelector('#swiper-wrapper');
var swiperSlide = Array.from(document.querySelectorAll('.rewies-item'));

var swiperCatalogCard = new Swiper('.rewies__container', {

	init : false,
	
	slidesPerView : 1,
	spaceBetween : 80,
	speed: 500,
	effect : 'slide',
	
	loop: true,
	// бесконечная прокрутка
	
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	
	on : {
	
		resize: function(){
	
			if(+container.offsetWidth > descWidth) {
	
				swiperWrapper.classList.add('g');
	
				swiperSlide.forEach( function(el){
					el.classList.remove('swiper-slide');
				});
	
				swiperCatalogCard.destroy();
				console.log('resize');
			}
		}
	},
	
});

if(+container.offsetWidth < descWidth){

	swiperWrapper.classList.remove('g');

	swiperSlide.forEach( function(el){
		el.classList.add('swiper-slide');
	});

	swiperCatalogCard.init();
}



window.addEventListener('resize', function(){


	if(+container.offsetWidth < descWidth){
	
		swiperWrapper.classList.remove('g');
		swiperSlide.forEach( function(el){
			el.classList.add('swiper-slide');
		});
	
		var swiperCatalogCard = new Swiper('.rewies__container', {
		
		    slidesPerView : 1,
			spaceBetween : 80,
		    speed: 500,
		    effect : 'slide',
	
		    loop: true,
		    // бесконечная прокрутка
	
		    navigation: {
		      nextEl: '.swiper-button-next',
		      prevEl: '.swiper-button-prev',
		    },
		
			on : {
	
				resize: function(){
	
					if(+container.offsetWidth > descWidth) {
	
						swiperWrapper.classList.add('g');
	
						swiperSlide.forEach( function(el){
							el.classList.remove('swiper-slide');
						});
	
						swiperCatalogCard.destroy();
						console.log('resize');
					}
	
				}
	
			},
	
		});
	
	}
	
});



})();
// end